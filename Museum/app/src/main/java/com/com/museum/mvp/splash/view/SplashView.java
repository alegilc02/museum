package com.com.museum.mvp.splash.view;

/**
 * Created by Alejandra Gil on 30/8/2019.
 */
public interface SplashView {
    void goToMain();
}
