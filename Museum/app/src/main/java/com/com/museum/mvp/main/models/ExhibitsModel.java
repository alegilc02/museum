package com.com.museum.mvp.main.models;

/**
 * Created by Alejandra Gil on 30/8/2019.
 */
public class ExhibitsModel {
    private String name;
    private Float rating;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
