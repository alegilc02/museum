package com.com.museum.mvp.main.presenter;

import com.com.museum.mvp.main.models.ExhibitsModel;
import com.com.museum.mvp.main.view.MainView;
import com.com.museum.utils.PreferencesManager;

import java.util.List;

/**
 * Created by Alejandra Gil on 30/8/2019.
 */
public class MainPresenter {
    private MainView view;

    public MainPresenter(MainView view) {
        this.view = view;
    }

    public void getDataLocal() {
        List<ExhibitsModel> exhibitsModels = PreferencesManager.getInstance().getListExhibits();
        if (exhibitsModels != null) {
            view.initRecicler(exhibitsModels);
        }

    }
}
