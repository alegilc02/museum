package com.com.museum.mvp.splash.presenter;

import android.os.Handler;

import com.com.museum.mvp.splash.view.SplashView;

/**
 * Created by Alejandra Gil on 30/8/2019.
 */
public class SplashPresenter {
    private SplashView view;

    public SplashPresenter(SplashView view) {
        this.view = view;
    }

    public void sleepView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                view.goToMain();
            }
        }, 2 * 1000);
    }
}
