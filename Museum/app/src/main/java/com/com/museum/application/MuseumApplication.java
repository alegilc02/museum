package com.com.museum.application;

import android.app.Application;
import android.content.Context;

/**
 * Created by Alejandra Gil on 30/8/2019.
 */
public class MuseumApplication extends Application {

    private static Context appContext;

    public static Context getContext() {
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        // initialize
        appContext = this;
    }
}
