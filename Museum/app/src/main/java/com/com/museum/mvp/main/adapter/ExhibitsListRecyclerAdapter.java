package com.com.museum.mvp.main.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.com.museum.R;
import com.com.museum.application.MuseumApplication;
import com.com.museum.mvp.detailExhibits.DetailExhibitsActivity;
import com.com.museum.mvp.main.MainActivity;
import com.com.museum.mvp.main.models.ExhibitsModel;
import com.com.museum.utils.GlobalData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

/**
 * Created by Alejandra Gil on 30/8/2019.
 */
public class ExhibitsListRecyclerAdapter extends RecyclerView.Adapter<ExhibitsListRecyclerAdapter.ViewHolder> {

    private List<ExhibitsModel> list;
    private  MainActivity mainActivity;

    public ExhibitsListRecyclerAdapter(List<ExhibitsModel> list, MainActivity mainActivity) {
        this.list = list;
        this.mainActivity = mainActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.exhibits_item_list, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ExhibitsModel exhibitsModel = list.get(position);
        holder.tittleText.setText(exhibitsModel.getName());
        holder.ratingBar.setMax(10);
        holder.ratingBar.setNumStars(10);
        holder.ratingBar.setRating(exhibitsModel.getRating());
        holder.floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalData.getInstance().setExhibitExhibitsDetail(exhibitsModel);
                Intent intent = new Intent(mainActivity, DetailExhibitsActivity.class);
                mainActivity.startActivityForResult(intent, 200);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tittleText;
        private RatingBar ratingBar;
        private FloatingActionButton floatingActionButton;

        ViewHolder(View v) {
            super(v);
            tittleText = v.findViewById(R.id.name_exhibits);
            ratingBar = v.findViewById(R.id.ratingBar_exhibits);
            floatingActionButton = v.findViewById(R.id.floatingActionButton);
        }
    }
}
