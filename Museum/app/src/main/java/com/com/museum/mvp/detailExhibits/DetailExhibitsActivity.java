package com.com.museum.mvp.detailExhibits;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.com.museum.R;
import com.com.museum.mvp.detailExhibits.presenter.DetailExhibitsPresenter;
import com.com.museum.mvp.detailExhibits.view.DetailExhibitsView;
import com.com.museum.mvp.main.models.ExhibitsModel;
import com.com.museum.utils.GlobalData;

public class DetailExhibitsActivity extends AppCompatActivity implements DetailExhibitsView {
    //************************** UI variables ***************************************
    //Buttons
    private Button buttonShare;
    private Button buttonSave;
    //EditText
    private EditText editTextName;
    //Rating
    private RatingBar ratingBarRating;
    //************************** Logical variables ***************************************
    private DetailExhibitsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_exhibits);
        init();
    }

    private void init(){
        //************************************ UI ****************************************
        //Buttons init
        buttonSave = findViewById(R.id.detail_exhibits_save_button);
        buttonShare = findViewById(R.id.detail_exhibits_share_button);
        //EditText init
        editTextName = findViewById(R.id.detail_exhibits_name_edit_text);
        //Rating init
        ratingBarRating = findViewById(R.id.detail_exhibits_rating_rating_bar);
        //************************************ Logic ****************************************
        presenter = new DetailExhibitsPresenter(this);

        initOnClickListeners();

        presenter.initInterface();
    }

    private void initOnClickListeners(){
        buttonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               presenter.share();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.saveAction();
            }
        });
    }


    @Override
    public void shareActionButton(String data){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, data);
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.choose)));
    }

    @Override
    public ExhibitsModel getDataFromInterface() {
        ExhibitsModel exhibitsModel = new ExhibitsModel();
        exhibitsModel.setRating(ratingBarRating.getRating());
        exhibitsModel.setName(editTextName.getText().toString());
        return  exhibitsModel;
    }

    @Override
    public void showToast(String data) {
        Toast toast = Toast.makeText(this, data, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onBackPressed() {
       closeActivity();
    }

    @Override
    public void closeActivity() {
        finish();
    }

    @Override
    public void setDataToInterface(ExhibitsModel exhibitsModel) {
        editTextName.setText(exhibitsModel.getName());
        ratingBarRating.setRating(exhibitsModel.getRating());
    }

    @Override
    public void showButtonShare(boolean visibility) {
        if(!visibility){
            buttonShare.setVisibility(View.GONE);
        }
        else
        {
            buttonShare.setVisibility(View.VISIBLE);
        }
    }
}
