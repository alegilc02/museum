package com.com.museum.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.com.museum.application.MuseumApplication;
import com.com.museum.mvp.main.models.ExhibitsModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Alejandra Gil on 30/8/2019.
 */
public class PreferencesManager {
    private SharedPreferences sharedPreferences;
    private String SHARED_PREFS_DATA = "data";

    private String SHARED_PREFS_LIST_KEY = "list";

    private static final PreferencesManager holder = new PreferencesManager();

    public static PreferencesManager getInstance() {
        return holder;
    }

    //-----------------------------------------Exhibits List---------------------------------------------------
    public void setListExhibits(List<ExhibitsModel> exhibitsModelList) {
        String json = new Gson().toJson(exhibitsModelList);
        Context context = MuseumApplication.getContext();
        sharedPreferences = context.getSharedPreferences(SHARED_PREFS_DATA,
                Context.MODE_PRIVATE);
        sharedPreferences.edit()
                .putString(SHARED_PREFS_LIST_KEY, json)
                .apply();
    }

    public List<ExhibitsModel> getListExhibits() {
        Type listType = new TypeToken<List<ExhibitsModel>>(){}.getType();
        Context context = MuseumApplication.getContext();
        sharedPreferences = context.getSharedPreferences(SHARED_PREFS_DATA,
                Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(SHARED_PREFS_LIST_KEY, null);
        return new Gson().fromJson(json,listType);
    }
}
