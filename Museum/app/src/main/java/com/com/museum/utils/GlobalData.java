package com.com.museum.utils;

import com.com.museum.mvp.main.models.ExhibitsModel;

import java.util.List;

/**
 * Created by Alejandra Gil on 30/8/2019.
 */
public class GlobalData {
    private static final GlobalData holder = new GlobalData();

    public static GlobalData getInstance() {
        return holder;
    }

    /**
     *
     */
    private ExhibitsModel exhibits;

    public ExhibitsModel getExhibitDetail() {
        return this.exhibits;
    }

    public void setExhibitExhibitsDetail(ExhibitsModel exhibits) {
        this.exhibits = exhibits;
    }

}
