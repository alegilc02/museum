package com.com.museum.mvp.detailExhibits.presenter;

import com.com.museum.mvp.detailExhibits.view.DetailExhibitsView;
import com.com.museum.mvp.main.models.ExhibitsModel;
import com.com.museum.utils.GlobalData;
import com.com.museum.utils.PreferencesManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Alejandra Gil on 2/9/2019.
 */
public class DetailExhibitsPresenter {
    private DetailExhibitsView view;
    private ExhibitsModel exhibitsModel;

    public DetailExhibitsPresenter(DetailExhibitsView view) {
        this.view = view;
    }

    public void share() {
        exhibitsModel = GlobalData.getInstance().getExhibitDetail();
        if (exhibitsModel != null) {
            if (exhibitsModel.getName() != null) {
                String data = String.format(Locale.ENGLISH,
                        "This exhibits: %s have this: %s rating",
                        exhibitsModel.getName(),
                        exhibitsModel.getRating());
                view.shareActionButton(data);
            }
        }
    }

    public void saveAction() {
        if (GlobalData.getInstance().getExhibitDetail() != null) {
            ExhibitsModel current = view.getDataFromInterface();
            current.setId(GlobalData.getInstance().getExhibitDetail().getId());
            updateExhibit(current);
        } else {
            createNewExhibits(view.getDataFromInterface());
        }
    }

    private void createNewExhibits(ExhibitsModel exhibitsModel) {
        List<ExhibitsModel> currentExhibitsList = PreferencesManager.getInstance().getListExhibits();
        if (currentExhibitsList == null) {
            currentExhibitsList = new ArrayList<>();
        }
        exhibitsModel.setId(currentExhibitsList.size() + 1);
        currentExhibitsList.add(exhibitsModel);
        PreferencesManager.getInstance().setListExhibits(currentExhibitsList);
        view.closeActivity();
    }

    private void updateExhibit(ExhibitsModel exhibitToEdit) {
        List<ExhibitsModel> currentExhibitsList = PreferencesManager.getInstance().getListExhibits();
        if (currentExhibitsList != null) {
            for (ExhibitsModel exhibitsModel : currentExhibitsList) {
                if (exhibitsModel.getId() == exhibitToEdit.getId()) {
                    exhibitsModel.setName(exhibitToEdit.getName());
                    exhibitsModel.setRating(exhibitToEdit.getRating());
                    PreferencesManager.getInstance().setListExhibits(currentExhibitsList);
                    view.showToast("Updated");
                    GlobalData.getInstance().setExhibitExhibitsDetail(null);
                    view.closeActivity();
                }
            }
        }
    }

    public void initInterface() {
        if (GlobalData.getInstance().getExhibitDetail() != null) {
            view.setDataToInterface(GlobalData.getInstance().getExhibitDetail());
            view.showButtonShare(true);
        } else {
            view.showButtonShare(false);
        }
    }
}
