package com.com.museum.mvp.detailExhibits.view;

import com.com.museum.mvp.main.models.ExhibitsModel;

/**
 * Created by Alejandra Gil on 2/9/2019.
 */
public interface DetailExhibitsView {
    void setDataToInterface(ExhibitsModel exhibitsModel);
    void showButtonShare(boolean visibility);
    void shareActionButton(String data);
    ExhibitsModel getDataFromInterface();
    void showToast(String data);
    void closeActivity();
}
