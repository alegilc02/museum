package com.com.museum.mvp.main.view;

import com.com.museum.mvp.main.models.ExhibitsModel;

import java.util.List;

/**
 * Created by Alejandra Gil on 30/8/2019.
 */
public interface MainView {
    void initRecicler(List<ExhibitsModel> exhibitsModels);
}
