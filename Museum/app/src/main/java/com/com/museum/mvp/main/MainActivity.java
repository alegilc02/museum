package com.com.museum.mvp.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.com.museum.R;
import com.com.museum.application.MuseumApplication;
import com.com.museum.mvp.detailExhibits.DetailExhibitsActivity;
import com.com.museum.mvp.main.adapter.ExhibitsListRecyclerAdapter;
import com.com.museum.mvp.main.models.ExhibitsModel;
import com.com.museum.mvp.main.presenter.MainPresenter;
import com.com.museum.mvp.main.view.MainView;
import com.com.museum.utils.PreferencesManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

/**
 * Created by Alejandra Gil on 30/8/2019.
 */
public class MainActivity extends AppCompatActivity implements MainView {
    private RecyclerView recipeReciclerView;
    private ExhibitsListRecyclerAdapter recyclerAdapter;
    private FloatingActionButton buttonAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        recipeReciclerView = findViewById(R.id.recicler_exhibits);
        buttonAdd = findViewById(R.id.main_add_button);
        MainPresenter presenter = new MainPresenter(this);
        presenter.getDataLocal();
        initOnClickListener();
    }

    private void initOnClickListener() {
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openExhibit();
            }
        });
    }

    private void openExhibit() {
        Intent intent = new Intent(MuseumApplication.getContext(), DetailExhibitsActivity.class);
        startActivityForResult(intent, 200);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 200) {
            initRecicler(PreferencesManager.getInstance().getListExhibits());
        }
    }

    @Override
    public void initRecicler(List<ExhibitsModel> exhibitsModels) {
        recipeReciclerView.setHasFixedSize(true);
        recipeReciclerView.setLayoutManager(new LinearLayoutManager(MuseumApplication.getContext()));
        recyclerAdapter = new ExhibitsListRecyclerAdapter(exhibitsModels, this);
        recipeReciclerView.setAdapter(recyclerAdapter);
    }
}
