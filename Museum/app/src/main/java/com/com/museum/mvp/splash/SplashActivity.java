package com.com.museum.mvp.splash;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.com.museum.R;
import com.com.museum.mvp.main.MainActivity;
import com.com.museum.mvp.splash.presenter.SplashPresenter;
import com.com.museum.mvp.splash.view.SplashView;

public class SplashActivity extends AppCompatActivity implements SplashView {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }

    private void init() {
        SplashPresenter presenter = new SplashPresenter(this);
        presenter.sleepView();
    }

    @Override
    public void goToMain() {
        Intent i = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
